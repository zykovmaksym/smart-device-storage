package max.smarthome.smartdevicestorage.repository

import max.smarthome.smartdevicestorage.repository.entity.DeviceEntity

interface DeviceRepository {
    fun findByMac(mac: String): DeviceEntity
    fun create(device: DeviceEntity)
}