package max.smarthome.smartdevicestorage.repository

import max.smarthome.smartdevicestorage.repository.entity.DeviceEntity
import org.springframework.stereotype.Repository

@Repository
class DeviceRepositoryImpl: DeviceRepository {
    private var devices: List<DeviceEntity> = listOf()

    override fun findByMac(mac: String): DeviceEntity = devices.find { it.mac == mac }!!

    override fun create(device: DeviceEntity) {
        devices = devices + device
    }
}