package max.smarthome.smartdevicestorage.repository.entity

import max.smarthome.smartdevicestorage.common.DeviceId
import max.smarthome.smartdevicestorage.model.DeviceType

data class DeviceEntity(
        val id: DeviceId,
        val type: DeviceType,
        val mac: String
)
