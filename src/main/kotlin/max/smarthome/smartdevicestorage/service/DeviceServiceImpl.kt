package max.smarthome.smartdevicestorage.service

import max.smarthome.smartdevicestorage.common.DeviceId
import max.smarthome.smartdevicestorage.model.Device
import max.smarthome.smartdevicestorage.model.DeviceMac
import max.smarthome.smartdevicestorage.model.DeviceWithId
import max.smarthome.smartdevicestorage.repository.DeviceRepository
import max.smarthome.smartdevicestorage.repository.entity.DeviceEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class DeviceServiceImpl(private val deviceRepository: DeviceRepository): DeviceService {
    override fun getDeviceByMac(deviceMac: DeviceMac): DeviceWithId {
        val deviceEntity = deviceRepository.findByMac(deviceMac.mac)
        return DeviceWithId(deviceEntity.id, deviceEntity.type, DeviceMac(deviceEntity.mac))
    }

    override fun create(device: Device): DeviceId {
        val deviceId = UUID.randomUUID()
        val deviceEntity = DeviceEntity(deviceId, device.type, device.mac.mac)
        deviceRepository.create(deviceEntity)
        return deviceId
    }
}