package max.smarthome.smartdevicestorage.service

import max.smarthome.smartdevicestorage.common.DeviceId
import max.smarthome.smartdevicestorage.model.Device
import max.smarthome.smartdevicestorage.model.DeviceMac
import max.smarthome.smartdevicestorage.model.DeviceWithId
import max.smarthome.smartdevicestorage.repository.DeviceRepository
import max.smarthome.smartdevicestorage.repository.entity.DeviceEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class DeviceServiceBeta(private val deviceRepository: DeviceRepository): DeviceService {
    override fun getDeviceByMac(deviceMac: DeviceMac): DeviceWithId {
        val deviceEntity = deviceRepository.findByMac(deviceMac.mac)
        return DeviceWithId(deviceEntity.id, deviceEntity.type, DeviceMac(deviceEntity.mac))
    }

    override fun create(device: Device): DeviceId {
        val deviceId = UUID.fromString("d9ef7352-8cf4-4fb8-ae4b-735926e8ef33")
        val deviceEntity = DeviceEntity(deviceId, device.type, device.mac.mac)
        deviceRepository.create(deviceEntity)
        return deviceId
    }
}