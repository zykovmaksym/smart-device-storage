package max.smarthome.smartdevicestorage.service

import max.smarthome.smartdevicestorage.common.DeviceId
import max.smarthome.smartdevicestorage.model.Device
import max.smarthome.smartdevicestorage.model.DeviceMac
import max.smarthome.smartdevicestorage.model.DeviceWithId

interface DeviceService {
    fun getDeviceByMac(deviceMac: DeviceMac): DeviceWithId
    fun create(device: Device): DeviceId
}