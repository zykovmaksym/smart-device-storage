package max.smarthome.smartdevicestorage

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SmartDeviceStorageApplication

fun main(args: Array<String>) {
    runApplication<SmartDeviceStorageApplication>(*args)
}
