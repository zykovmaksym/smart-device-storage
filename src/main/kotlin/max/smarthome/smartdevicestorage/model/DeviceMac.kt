package max.smarthome.smartdevicestorage.model

data class DeviceMac(
        val mac: String
)
