package max.smarthome.smartdevicestorage.model

import max.smarthome.smartdevicestorage.common.DeviceId

data class DeviceWithId(
        val id: DeviceId,
        val type: DeviceType,
        val mac: DeviceMac
)
