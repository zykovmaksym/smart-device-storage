package max.smarthome.smartdevicestorage.model

import max.smarthome.smartdevicestorage.common.DeviceId

data class Device(
        val type: DeviceType,
        val mac: DeviceMac
)
