package max.smarthome.smartdevicestorage.controller

import max.smarthome.smartdevicestorage.controller.api.GetDeviceResponse
import max.smarthome.smartdevicestorage.controller.api.RegisterDeviceRequest
import max.smarthome.smartdevicestorage.controller.api.RegisterDeviceResponse
import max.smarthome.smartdevicestorage.model.Device
import max.smarthome.smartdevicestorage.model.DeviceMac
import max.smarthome.smartdevicestorage.model.DeviceType
import max.smarthome.smartdevicestorage.service.DeviceService
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api")
class DeviceController(@Qualifier("deviceServiceBeta") private val deviceService: DeviceService) {
    @GetMapping("/device/{mac}")
    fun fetchDeviceByMac(@PathVariable mac: String): ResponseEntity<GetDeviceResponse> {
        val device = deviceService.getDeviceByMac(DeviceMac(mac))
        val response = GetDeviceResponse(device.id, device.type.name)
        return ResponseEntity.ok(response)
    }

    @PostMapping("/device/register")
    fun registerDevice(@RequestBody request: RegisterDeviceRequest): ResponseEntity<RegisterDeviceResponse> {
        val deviceId = deviceService.create(Device(DeviceType.valueOf(request.type), DeviceMac(request.mac)))
        val response = RegisterDeviceResponse(deviceId)
        return ResponseEntity.ok(response)
    }
}