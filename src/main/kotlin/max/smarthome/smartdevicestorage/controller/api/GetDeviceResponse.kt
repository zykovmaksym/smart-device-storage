package max.smarthome.smartdevicestorage.controller.api

import java.util.*

data class GetDeviceResponse(
        val id: UUID,
        val type: String
)
