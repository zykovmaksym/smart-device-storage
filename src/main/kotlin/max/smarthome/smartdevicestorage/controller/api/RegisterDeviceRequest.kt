package max.smarthome.smartdevicestorage.controller.api

data class RegisterDeviceRequest(
        val type: String,
        val mac: String
)
