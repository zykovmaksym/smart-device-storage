package max.smarthome.smartdevicestorage.controller.api

import java.util.*

data class RegisterDeviceResponse(
        val deviceId: UUID
)
